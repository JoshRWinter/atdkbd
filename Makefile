obj-m += atdkbd.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	rm atdkbd.ko atdkbd.mod.c atdkbd.mod.o atdkbd.o modules.order Module.symvers

